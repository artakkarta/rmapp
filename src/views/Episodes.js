import React , {useState, useEffect } from 'react';

import ClickableTable from './../components/ClickableTable'
import TitleSearchBar from './../components/TitleSearchBar'
import CharacterListDialog from './../components/CharacterListDialog'
import PaginationBar from './../components/PaginationBar'

import * as RMAPI from './../services/RMAPIConnector.js'
import useLocalStorage from './../services/LocalStorage.js'

export default function EpisodesList() {
	
	//-------- Episodes table parameters -----------------------
	//----------------------------------------------------------
	
	//Declare episodes list to be rendered
	const [episodes,setEpisodes] = useState(RMAPI.emptyEpisode);
	// Favorite episode to be sotred in localStorage
	const [favorites,setFavorites] = useLocalStorage("favEpisodes",[]);
	// Paging information retreived from the API calls
	const [pageInfo,setPageInfo] = useState([]);
	// Current page number
	const [curPage,setCurPage] = useState(0);
	// list of column titles for episodes table
	const cols = ["episode","name","air_date"];
	//Current selected row (current Episode parameters)
	const [selectedRow,setSelectedRow] = useState(RMAPI.emptyEpisode);
	
	
	//-------- Characters diualog parameters -------------------
	//----------------------------------------------------------
	
	//List of characters of current épisodes to be rendered
	const [characters,setCharacters] = useState([]);
	//Used for filling character one by one before setting the state
	let tmpChar = [];
	//Managing dialog open and close render
	const [dialogOpen, setDialogOpen] = React.useState(false);
	
	
	// at loading get episodes from API and fill obj state
	useEffect(() => {
		RMAPI.fetchEpisodesFromUrl("https://rickandmortyapi.com/api/episode",favorites).then(res => {
			//console.log(res)
			setEpisodes(res.results);
			setPageInfo(res.info);
			setCurPage(1);
		});
	}, [favorites]);
	
	//-------- Episodes Table and toolbar handlers--------------
	//----------------------------------------------------------
	
	const handleSearchClick = (searchValue) => {
		//console.log ('Search : '+searchValue);
		RMAPI.fetchEpisodesFiltered(searchValue,favorites).then(res => {
			//console.log(res)
			setEpisodes(res.results);
			setPageInfo(res.info);
			setCurPage(1);
		});
	}
	
	const handleNextPageClick = () => {
		RMAPI.fetchEpisodesFromUrl(pageInfo.next,favorites).then(res => {
			setEpisodes(res.results);
			setPageInfo(res.info);
			setCurPage(curPage+1);
		});
	}
	
	const handlePreviousPageClick = () => {
		RMAPI.fetchEpisodesFromUrl(pageInfo.prev,favorites).then(res => {
			setEpisodes(res.results);
			setPageInfo(res.info);
			setCurPage(curPage-1);
		});
	}
	
	const handleFavoriteClick = (event,row) => {

		let i=favorites.indexOf(row.episode)
		const fav=favorites;
		if(i>=0)
			fav.splice(i, 1);
		else
			fav.push(row.episode);
		setFavorites(fav);
		
		// calling map sans reconstruir le setEpisode sinon trop de render? A VERIFIER
		episodes.map((ep) => {
			if(ep.episode === row.episode){
				console.log(ep);
				if(i>=0)
					ep.fav=false;
				else
					ep.fav=true;
			}
			return ep;
		})
		
		console.log(episodes);
	}
	
	//------------ Characters dialog handlers-------------------
	//----------------------------------------------------------
	
	//Function to be given to the table object to handle the click of one row
	const handleRowClick = (row) => {
		//Set the current row
		setSelectedRow(row);
		//console.log(row);
		if(row && row.characters && row.characters.length>0)
		{
			//console.log(selectedRow.characters.length);
			
			row.characters.map(char => (
				//fetch any character from the current episode 
				RMAPI.fetchCharacterFromUrl(char).then(result => {

					// push characters into the temporary character list
					tmpChar.push(result);
					
					// Render only once while the character list is filled
					if(tmpChar.length>=row.characters.length){
						setCharacters(tmpChar);
						tmpChar = [];
					}
				})
			));
		}
		//Render dialog
		setDialogOpen(true);
	}
	
	//Render closing dialog and empty all character lists
	const handleCloseDialog = () => {
		setDialogOpen(false);
		setCharacters([]);
		tmpChar=[];
	};
	
	//------------ RENDERING -----------------------------------
	//----------------------------------------------------------
	
	return (
			
			<div>
				{/* TITLE BAR WITH SEARCH*/}
				<TitleSearchBar title="Rick & Morty APP (RMAPP) for Job Interview" handleSearch={handleSearchClick} />
				{/* EPISODES DATA TABLE */}
				<ClickableTable columns={cols} rows={episodes} handlerRowClick={handleRowClick} handlerCheckboxChange={handleFavoriteClick}/>		
				{/* BOTTOM BAR WITH PAGE NAVIGATION */}
				<PaginationBar rows={episodes.length} pages={pageInfo.pages} page={curPage} handlePrev={handlePreviousPageClick} handleNext={handleNextPageClick}/>	
				{/* DIALOG FOR CHARACTERS LIST */}
				<CharacterListDialog dialogOpen={dialogOpen} data={selectedRow} characterList={characters} handleClose={handleCloseDialog}/>
			</div>
	);	

}