import React from 'react';

import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';

import EpisodesList from './views/Episodes'
import logo from './marmotte.jpg';
import './App.css';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://www.linkedin.com/in/cattaluca/">
        Luca Cattaneo
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
	  <img src={logo} class='App-logo' alt='logo'/>
    </Typography>
  );
}

function App() {
	return (
		<div className="App">
			
			<EpisodesList/>
			
			<Box pt={4}>
				<Copyright />
			</Box>

		</div>
	);
}

export default App;
