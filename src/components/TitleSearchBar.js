import React from 'react';


import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import SearchIcon from '@mui/icons-material/Search';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';

import InputBase from '@mui/material/InputBase';
import { styled, alpha } from '@mui/material/styles';

//------ Styles for Searchbar from MUI tuto ----------------
//----------------------------------------------------------
	
export const Search = styled('div')(({ theme }) => ({
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: alpha(theme.palette.common.white, 0.15),
		'&:hover': {
			backgroundColor: alpha(theme.palette.common.white, 0.25),
		},
		marginLeft: 0,
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			marginLeft: theme.spacing(1),
			width: 'auto',
		},
	}));
	
export const StyledInputBase = styled(InputBase)(({ theme }) => ({
		color: 'inherit',
		'& .MuiInputBase-input': {
			padding: theme.spacing(1, 1, 1, 0),
			// vertical padding + font size from searchIcon
			paddingLeft: `calc(1em + ${theme.spacing(4)})`,
			transition: theme.transitions.create('width'),
			width: '100%',
			[theme.breakpoints.up('sm')]: {
				width: '12ch',
				'&:focus': {
					width: '20ch',
				},
			},
		},
	}));
	
//------------- TitleSearchBar Component -------------------
//----------------------------------------------------------

export default function TitleSearchBar({title,handleSearch}) {

	//Store search bar value
	let searchValue = "";

	// On search bar change fill search bar value
	const handleSearchChange = (event) => {
		searchValue = event.target.value;
	}
	
	const handleKey = (event) => {
		if(event.keyCode === 13)
			handleSearch(searchValue);
	}

	return (
		<Box sx={{ flexGrow: 1 }}>
			<AppBar position="static">
				<Toolbar>
					<Typography
						variant="h6"
						noWrap
						component="div"
						sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
					>
					{title}
					</Typography>
					<Search>
						<StyledInputBase
							placeholder="Search…"
							onChange={handleSearchChange}
							onKeyDown={handleKey}
						/>
					</Search>
					<IconButton sx={{
						p : '2, 2',
						color : 'white'
					}}
					onClick={() => handleSearch(searchValue)}
					>
						<SearchIcon />
					</IconButton>
				</Toolbar>
			</AppBar>
		</Box>
	)

}