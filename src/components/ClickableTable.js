import React, {useState} from 'react';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';

import { styled } from '@mui/material/styles';

export default function ClickableTable({columns,rows,handlerRowClick,handlerCheckboxChange}) {
	
	//To store the dynamic status of the checkboxes
	const [isChecked, setIsChecked] = useState([]);
		
	// Used to manage the instantaneus render of the Checkboxes
	const handleCheckBoxState = (event,row,index) => {
		
		setIsChecked(isChecked.map((ch, i) => {
			 if(i===index){
				if(ch===true)
					return false
				else
					return true
			 }
			 return ch;
		}));
		
		//call the real data handler
		handlerCheckboxChange(event,row);
	}
	
	//Styles for the table
	const StyledTableCell = styled(TableCell)(({ theme }) => ({
		[`&.${tableCellClasses.head}`]: {
			fontSize: 14,
			fontWeight: "bold",
		},
		[`&.${tableCellClasses.body}`]: {
			fontSize: 12,
		},
	}));
	
	//Create DOM titles according to props
	const domTitle = () => {
		// console.log("titles.length: "+titles.length)
		if (columns && columns.length >0){
			// console.log("titles: "+titles);
			return(columns.map((col,index) => (
				<StyledTableCell key={index}>{col}</StyledTableCell>
			)));
		}
	}

	//Create DOM table lines according to props
	const domTable = () => {
		//console.log(rows);
		if (rows && rows.length >0){
			return(rows.map((row,index) => {
				
				// Fill isChecked with favorite parameter from the episode
				isChecked[index]=row.fav;
				
				return(
				<TableRow key={index}>
					<TableCell padding="checkbox">
						<Checkbox
							key={index}
							color="primary"
							onChange={(event) => handleCheckBoxState(event,row,index)}
							checked={isChecked[index]}
						/>
					</TableCell>
					{
						columns.map((col,index) => (
							<StyledTableCell 
								key={index}
								onClick={() => handlerRowClick(row)}
							>
								{row[col]}
							</StyledTableCell>
						))
					}
				</TableRow>
				)
			}
			));
		}
	}
  
	return (
		<div>
			<TableContainer component={Paper}>	
				<Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
					<TableHead>
						<TableRow>
							<StyledTableCell>Favorites</StyledTableCell>
							{domTitle()}
						</TableRow>
					</TableHead>
					<TableBody>
						{domTable()}
					</TableBody>
				</Table>
			</TableContainer>
			
		</div>
	);
}