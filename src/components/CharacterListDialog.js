import React from 'react';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import Button from '@mui/material/Button';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import ArrowForward from '@mui/icons-material/ArrowForward';

export default function CharacterListDialog({dialogOpen,data,characterList,handleClose}) {
	
	//create the DOM of the character list
	const domCharactersList = () => {
		//console.log(characters.length);
		//console.log(characters);
		if(characterList && characterList.length)
		{
			return(characterList.map((char,i) => (
				<ListItem disablePadding key={i}>
					<ListItemIcon>
						<ArrowForward />
					</ListItemIcon>
					<ListItemText primary={char.name} />
				</ListItem>
			)));
		}
	}

	return (
		<Dialog
		open={dialogOpen}
		onClose={handleClose}
		>
			<DialogTitle id="alert-dialog-title">Episode {data.episode} :</DialogTitle>
			<DialogContent>
				<DialogContentText id="alert-dialog-description">
					<Typography sx={{ mt: 0, mb: 2 }} variant="body2">
						<b>Name : </b>{data.name}
						<br/>
						<b> On air : </b>{data.air_date}
					</Typography>
					<Typography sx={{ mt: 1, mb: 0 }} variant="h6">
					Characters list :
					</Typography>
				</DialogContentText>
				<List>
					{ domCharactersList() }
				</List>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleClose} color="primary">
					Close
				</Button>
			</DialogActions>
		</Dialog>
	)

}