import React from 'react';

import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';

export default function PaginationBar({pagesInformations,rows,pages,page,handlePrev,handleNext}) {

	return (
		<Box sx={{ flexGrow: 1 }}>
			<AppBar position="static">
				<Toolbar align="right">
					<Typography
					variant="body2"
					sx={{ 
						flex: '1',
						mr : 2	//margin right
					}}
					>
						Rows per page : {rows}
					</Typography>
					<Divider orientation="vertical" flexItem />
					<Typography
					variant="body2"
					sx={{ 
						//width: '100%',
						mr : 2,	//margin right
						ml : 2	//margin left
					}}
					>
						Page {page} of {pages}
					</Typography>
					<Divider orientation="vertical" flexItem />
					<Tooltip title="Previous page">
						<IconButton sx={{
							p : '2, 2',
							color : 'white',
							ml : 2	//margin left
						}}
						onClick={handlePrev}>
							<KeyboardArrowLeftIcon />
						</IconButton>
					</Tooltip>
					<Tooltip title="Next page">
						<IconButton sx={{
							p : '2, 2',
							color : 'white'
						}} 
						onClick={handleNext}>
							<KeyboardArrowRightIcon />
						</IconButton>
					</Tooltip>
				</Toolbar>
			</AppBar>
		</Box>
	)

}