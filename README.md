# README #

Rick and Morty APP from Luca CATTANEO's 

### RMAPP ###

Rick and Morty APP (RMAPP) is written for [Luca CATTANEO](https://www.linkedin.com/in/cattaluca/)'s Job Interview.

The application is available here: [open RMAPP](http://176.133.124.221)

### Design ###

App is written using [React](https://fr.reactjs.org/),  [MaterialUI](https://mui.com/) and [Rick and Morty API](https://rickandmortyapi.com/).

RMAPP is a single page APP using the following elements :

* services/RMAPIConnector.js : a set of tools allowing to ease the acces to Rick and Morty API 

* services/LocalStorage.js : override of useState() to store values in Windows localStorage

* views/Episodes.js : The one and only view using the connector to fill all componenets of the APP

* components : A set of componenets allows to display and navigate the data

### Q&A ###

1. Décrivez l’intérêt de React par rapport à du JS / HTML classique (par ex. jQuery) à quelqu’unqui n’y connaît rien en développement informatique.

React est une évolution du langage JQuery, il est plus performant et il est fait pour la creation de app complexe, là ou JQuery est orienté vers l'écriture de sites Web.

* React parts des données (Objects) pour aller créer les pages Web (DOM) alors que JQuery parts de pages web (DOM) pour créer les données de l'APP (Obj)

* La façon d'écrire du code (Syntaxe) React diverge de langages traditionnels

* Les modifications de pages Web (DOM) sont automatiquement minimisé et optimisé dans React

2. En PHP, et de manière générale en programmation orientée objet, à quoi servent les interfaces ?

Une interface est l'ensemble des methodes publiques definissant une classe ou un ensemble de classes. Elle permettent d'abstraire le code et de séparer l'implementation et la définition.

3. Bill Gates a dit “Je recrute toujours les développeurs les plus fainéants possible, car je sais qu’il produiront du code qui offre un maximum de résultat pour un minimum d’effort”. Qu’en pensez-vous ?

Comme toute idéologie c'est à prendre avec modération, cela est vrai car un bon développeurs sait créer des outils pour automatiser ses taches, ou factoriser son code pour eviter d'écrire plusieurs fois la meme chose.
Pourtant aujord'hui avec la complexité des systèmes il est necessaire de faire un pas en arrière et suivre certaines regle de programmation qui peuvent rendre le développement plus long (reviews, tests, docs..).
Pour resumer d'apres moi cette citation commence à vieillir.