//Set of function to connect to Reick & Mort API
// allows to ease use of async functions with React

export let fetchCharacterFromUrl, fetchEpisodesFromUrl, fetchEpisodesFiltered, addForFavorites, emptyResponseData, emptyEpisode;

fetchEpisodesFromUrl = async function fetchEpisodesFromUrl(url,favorites) {
	
	if(url === "")
		url="https://rickandmortyapi.com/api/episode";
	
	let response = await fetch(url);	
	let data = await response.json();
	
	//return the data if results exists
	if(data.results){
		
		//map output data with favorites and add fav item
		addForFavorites(data.results,favorites);
		return data;	
	}

	//if no data return empty Response
	return emptyResponseData;
}

fetchEpisodesFiltered = async function fetchEpisodesFiltered(filter,favorites) {
	
	let url="https://rickandmortyapi.com/api/episode/?name="+filter;
	
	let response = await fetch(url);	
	let data = await response.json();
	
	//console.log(data);
	
	//return the data if results exists
	if(data.results){
		//map output data with favorites and add fav item
		addForFavorites(data.results,favorites);
		return data;	
	}

	//if no data return empty Response
	return emptyResponseData;
}

fetchCharacterFromUrl = async function fetchCharacterFromUrl(url) {

	let response = await fetch(url);
	let data = await response.json()

	if(data)
		return data;
	else
		return emptyResponseData;
}

addForFavorites = function addForFavorites(episodes,favorites) {
	//map output data with favorites and add fav item
	episodes.map((ep) => {
		let i=favorites.indexOf(ep.episode);
		//console.log(ep);
		if(i>=0)
			ep.fav=true;
		else
			ep.fav=false;
		return ep;
	});
}

//empty structure of prmise response from the API, useful when initalising
emptyResponseData = {"info": {"count":0,"pages":0,"next":null,"prev":null},"results":[]};

//empty structure of prmise response from the API, useful when initalising
//Favorite information is added for using in the APP
emptyEpisode = {"id":null,"episode":null,"name":null,"characters":[],"fav":false};
